/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additiUnable toUnable to connect to buttonn * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// {{{ Bluetooth

/**
 * Called when bluetooth is initialized.
 */
function initialized(result)
{
    if (result.status == 'enabled') {
        console.log('Bluetooth is enabled.');
    } else {
        console.log('Bluetooth is not enabled');
    }
}

/**
 *
 */
function connectTimeout(data, reject)
{
    console.log('Connecting timed out, disconnecting');

    bluetoothle.disconnect(
        function() {
            console.log('Connect failed, attempt aborted');
            reject();
        },
        function() {
            console.log('Connect failed, attempt not aborted');
            reject();
        },
        {"address": data.address});
}

/**
 *
 */
function connect(data)
{
    console.log(JSON.stringify(data));

    return new Promise(function(resolve, reject) {
        // Stop scanning when trying to connect
        stopScan();

        // Clear existing timeout if any exists.
        if (window._connect_timeout) {
            clearTimeout(window._connect_timeout);
        }

        // Add a 10 second connect timeout.
        window._connect_timeout = setTimeout(
            function() {
                connectTimeout(data, reject);
            },
            10000);

        console.log('Attempting to connect to ' + data.address);

        var params = {"address": data.address};

        bluetoothle.wasConnected(
            function(data2) {
                if (data2.wasConnected === true) {
                    bluetoothle.reconnect(
                        function(data) {
                            console.log('Reconnected to ' + data.address);
                            onConnect(data, resolve, reject);
                        },
                        function() {
                            console.log('Failed to reconnect to ' + data.address);
                            clearTimeout(window._connect_timeout);   
                            reject();
                        },
                        params);
                } else {
                    bluetoothle.connect(
                        function(data) {
                            console.log('Connected to ' + data.address);
                            onConnect(data, resolve, reject);
                        },
                        function() {
                            console.log('Failed to connect to ' + data.address);
                            clearTimeout(window._connect_timeout);   
                            reject();
                        },
                        params);
                }
            },
            function() {
                bluetoothle.connect(
                    function(data) {
                        console.log('Connected to ' + data.address);
                        onConnect(data, resolve, reject);
                    },
                    function() {
                        console.log('Failed to connect to ' + data.address);
                        clearTimeout(window._connect_timeout);   
                        reject();
                    },
                    params);
            },
            params);
    });
}

/**
 *
 */
function onConnect(data, resolve, reject)
{
    clearTimeout(window._connect_timeout);

    switch (data.status) {
        case 'connected':
            console.log('Connected to device ' + data.address);

            window._device = data;

            bluetoothle.discover(
                function(data) {
                    onDiscover(data, resolve, reject);
                },
                function() {
                    servicesError(data, reject);
                },
                {'address': data.address});
            break;

        case 'disconnected':
            console.log('Disconnected from device ' + data.address);

            close(window._device.address)
                .then(function() {
                    var evnt = new CustomEvent(
                        'deviceDisconnected',
                        {detail: {
                            device: {
                                address: window._device.address,
                                name: window._device.name,
                            }
                        }});
                    window.dispatchEvent(evnt);
            
                    window._device = undefined;
                })
                .catch(function() {
                    var evnt = new CustomEvent(
                        'deviceDisconnected',
                        {detail: {
                            device: {
                                address: window._device.address,
                                name: window._device.name,
                            }
                        }});
                    window.dispatchEvent(evnt);
            
                    window._device = undefined;
                });

            break;
    } 
}

/**
 *
 */
function getCharacteristic(data)
{
    for (var i = 0; i < data.services.length; i++) {
        if (data.services[i].uuid.indexOf('8331') == 4) {
            for (var x = 0; x < data.services[i].characteristics.length; x++) {
                if (data.services[i].characteristics[x].uuid.indexOf('8311') == 4) {
                    var service_id = data.services[i].uuid;
                    var characteristic_id = data.services[i].characteristics[x].uuid;

                    return {
                        'address': data.address,
                        'service': service_id,
                        'characteristic': characteristic_id,
                    };
                }
            }
        }
    }

    return undefined;
}

/**
 *
 */
function onDiscover(data, resolve, reject)
{
    console.log('Got services for device ' + data.address);

    bluetoothle.subscribe(
        function(data) {
            subscribeSuccess(data, resolve);
        },
        function() {
            console.log('Unable to get service info for connected device');

            disconnect(data.address)
                .then(function() {
                    reject();
                })
                .catch(function() {
                    reject();
                });
        },
        getCharacteristic(data));
}

/**
 *
 */
function subscribeSuccess(data, resolve)
{
    if (data.status == 'subscribed') {
        console.log('Subscribed to device notifications');

        // Store the device address of the last device we successfully 
        // connected to.
        window.localStorage.setItem('device', window._device.address);

        console.log('Calling resolve method');

        resolve(window._device);

        var evnt = new CustomEvent(
            'deviceConnected',
            {detail: {
                device: {
                    address: window._device.address,
                    name: window._device.name,
                }
            }});
        window.dispatchEvent(evnt);

        return;
    }

    console.log('Notification received');

    if (data.value == 'AA==') {
        console.log('Button released');
        var button_press_time = new Date().getTime() - window._button_push_time;
        window._button_push_time = 0;

        window._button_unpress_time = new Date().getTime()

        console.log('Button press time ' + button_press_time);
    } else if (data.value == 'AQ==') {
        console.log('Button pressed');
        window._button_push_time = new Date().getTime();

        var time_since_last_press = window._button_push_time - window._button_unpress_time;

        console.log('Time since last press ' + time_since_last_press);

        window._button_unpress_time = 0
    } 
}

/**
 *
 */
function scan(stop_when_found)
{
    return new Promise(function(resolve, reject) {
        console.log('Starting scan for devices');

        bluetoothle.startScan(
            function(data) {
                scanDataReceived(data, resolve, reject, stop_when_found);
            },
            function() {
                console.log('Starting scan failed');

                var evnt = new CustomEvent('scanStartFailed');
                window.dispatchEvent(evnt);

                reject();
            });
    });
}

/**
 *
 */
function stopScan()
{
    return new Promise(function(resolve, reject) {
        console.log('Stopping scan for devices');

        bluetoothle.isScanning(function(data) {
            if (data.isScanning === true) {
                bluetoothle.stopScan(
                    function(data) {
                        console.log('Scan stopped');

                        if (window._scan_timeout) {
                            clearTimeout(window._scan_timeout);
                        }

                        var evnt = new CustomEvent('scanStopped');
                        window.dispatchEvent(evnt);

                        resolve();
                    },
                    function() {
                        console.log('Scan stop failed');

                        var evnt = new CustomEvent('scanStopFailed');
                        window.dispatchEvent(evnt);

                        reject();
                    });
            } else {
                console.log('Not stopping scan because is not running');
            }
        });
    });
}

/**
 *
 */
function scanTimeout(resolve, reject, stop_when_found)
{
    console.log('Scan timeout reached. Stopping scan...');

    if (stop_when_found === true) {
        stopScan().then(reject);
    } else {
        stopScan().then(resolve);
    }
}

/**
 *
 */
function scanDataReceived(data, resolve, reject, stop_when_found)
{
    if (data.status == 'scanStarted') {
        console.log('Scan started, setting 10 second timeout.');

        if (window._scan_timeout) {
            clearTimeout(window._scan_timeout);
        }

        window._scan_timeout = setTimeout(
            function() {
                scanTimeout(resolve, reject, stop_when_found);
            }, 10000);

        var evnt = new CustomEvent('scanStarted');
        window.dispatchEvent(evnt);
    } else if (data.status == 'scanResult') {
        if (data.name != 'Sensibel 4.3.5') {
            console.log('Device found "' + data.name + '" ' + data.address);
            return;
        }

        if (stop_when_found === true && window._scan_timeout) {
            clearTimeout(window._scan_timeout);
        }

        if (stop_when_found === true) {
            resolve(data);
        }

        var evnt = new CustomEvent(
            'deviceFound',
            {detail: {device: data}});
        window.dispatchEvent(evnt);
    }
}

/**
 * Disconnect from the provided address.
 */
function disconnect(address)
{
    return new Promise(function(resolve, reject) {
        console.log('Attempting to disconnect from device ' + address);

        var params = {
            'address': address,
        };

        bluetoothle.isConnected(
            function(data) {
                if (data.isConnected === true) {
                    console.log('Device is connected, attempt disconnect');

                    bluetoothle.disconnect(
                        function() {
                            console.log('Disconnected from device ' + address + ' successfully');
                            resolve(address);

                            var evnt = new CustomEvent(
                                'deviceDisconnected',
                                {detail: {
                                    device: {
                                        address: window._device.address,
                                        name: window._device.name,
                                    }
                                }});
                            window.dispatchEvent(evnt);

                            window._device = undefined;
                        },
                        function() {
                            console.log('Could not disconnect from device ' + address);
                            reject();

                            var evnt = new CustomEvent(
                                'deviceDisconnected',
                                {detail: {
                                    device: {
                                        address: window._device.address,
                                        name: window._device.name,
                                    }
                                }});
                            window.dispatchEvent(evnt);
                        },
                        params); 
                } else {
                    console.log('Device is not connected, not disconnecting');
                }
            },
            function() {
                console.log('Unable to determine if device is connected');
                reject();
            },
            params);

    });
}

/**
 * Close from the provided address.
 */
function close(address)
{
    return new Promise(function(resolve, reject) {
        console.log('Attempting to close connection to device ' + address);

        var params = {
            'address': address,
        };

        bluetoothle.close(
            function() {
                console.log('Closed connection to device ' + address + ' successfully');
                resolve(address);

                var evnt = new CustomEvent(
                    'deviceClosed',
                    {detail: {device: {address: address}}});
                window.dispatchEvent(evnt);
            },
            function() {
                console.log('Could not close connection to device ' + address);
                reject();

                var evnt = new CustomEvent(
                    'deviceClosed',
                    {detail: {device: {address: address}}});
                window.dispatchEvent(evnt);
            },
            params);
    });
} 

// }}}

// {{{ UI helpers

/**
 *
 */
function addDeviceToTable(device, connected)
{
    if (connected === true) {
        var is_connected = 'Yes';
    } else {
        var is_connected = 'No';
    }   

    var html = [
        '<tr data-address="' + device.address + '">',
        '<td>' + device.name + '</td>',
        '<td>' + device.address + '</td>',
        '<td>' + is_connected + '</td>'
    ];

    if (is_connected == 'Yes') {
        html.push('<td><button id="disconnect">Disconnect</button></td>');
    } else {
        html.push('<td><button id="connect">Connect</button></td>');
    }

    html.push('</tr>');

    // If this device is already in the table then remove it
    removeDeviceFromTable(device.address);

    // Add the device into the table
    $('#devices #no-devices').remove();
    $('#devices tbody').append(html.join(''));

    // Add event listeners
    $('#devices tbody tr').each(function() {
        if ($(this).data('address') == device.address) {
            $(this).find('#disconnect').click(disconnectDeviceUI);
            $(this).find('#connect').click(connectDeviceUI);
        }
    });
}

/**
 *
 */
function removeDeviceFromTable(address)
{
    $('#devices tbody tr').each(function() {
        if ($(this).data('address') == address) {
            $(this).remove();
        }
    });   
}

/**
 * Disconnect from the device. Update row in table regardless.
 */
function disconnectDeviceUI()
{
    var address = $(this).parents('tr').attr('data-address');

    disconnect(address)
        .then(function() {
            navigator.notification.alert(
                'Disconnected from ' + address,
                function() {},
                'Disconnected',
                'OK');
        })
        .catch(function() {
            navigator.notification.alert(
                'Unable to disconnect from ' + address,
                function() {},
                'Unable to disconnect',
                'OK');
        });
}

/**
 * Connect to device
 */
function connectDeviceUI()
{
    var address = $(this).parents('tr').data('address');

    if (window._device) {
        navigator.notification.alert(
            'Please disconnect from other device first',
            function() {},
            'Already connected',
            'OK');
    } else {
        connect({"address": address})
            .then(function() {
                navigator.notification.alert(
                    'Connected to ' + address,
                    function() {},
                    'Connected',
                    'OK');
            })
            .catch(function() {
                navigator.notification.alert(
                    'Failed to connect to ' + address,
                    function() {},
                    'Connect Failed',
                    'OK');
            })
    }
}

/**
 *
 */
function getDeviceForJourney()
{

    return new Promise(function(resolve, reject) {
        function has_connected(device) {
            console.log('Connected to device ' + device.address + ' - can start journey');

            window.addEventListener(
                'deviceDisconnected',
                reconnectDeviceForJourney);

            resolve(device);
        }

        function attempt_scan() {
            console.log(
                'Connecting to stored device address failed. Attempting to scan for other devices');

            scan()
                .then(connect)
                .then(has_connected)
                .catch(connect_failed);
        }

        function connect_failed() {
            console.log('Unable to find any buttons');
            reject();
        }

        var device = window.localStorage.getItem('device');

        connect({address: device})
            .then(has_connected)
            .catch(attempt_scan);
    });
}

/**
 *
 */
function reconnectDeviceForJourney(evnt)
{
    function reconnect() {
        var params = {
            'address': evnt.detail.device.address,
        };

        bluetoothle.isConnected(
            function(data) {
                if (data.isConnected !== true) {
                    console.log('Attempting to reconnect to device ' + evnt.detail.device.address);

                    connect({address: evnt.detail.device.address})
                        .then(has_connected)
                        .catch(retry_connect);
                }
            },
            function() {
                console.log('Attempting to reconnect to device ' + evnt.detail.device.address);

                connect({address: evnt.detail.device.address})
                    .then(has_connected)
                    .catch(retry_connect);
            },
            params);
    }

    function has_connected(device) {
        console.log('Reconnected to device ' + device.address + ' can resume journey');

        window.addEventListener(
            'deviceDisconnected',
            reconnectDeviceForJourney);

        resolve(device);
    } 

    function retry_connect() {
        window._journey_reconnect_timeout = setTimeout(reconnect, 20000);
    }

    reconnect();
}

// }}}

/**
 * Device ready callback.
 */
function deviceReady() {
    console.log('Device ready');

    // Show scanning dialog when scanning
    window.addEventListener('scanStarted', function() {
        $('#scanningDialog').show();
    });

    // Hide scanning dialog when scanning stopped.
    window.addEventListener('scanStopped', function() {
        $('#scanningDialog').hide();
    });

    // When device is found add into table
    window.addEventListener('deviceFound', function(evnt) {
        addDeviceToTable(evnt.detail.device, false);
    });

    window.addEventListener('deviceConnected', function(evnt) {
        addDeviceToTable(evnt.detail.device, true);
    });

    window.addEventListener('deviceDisconnected', function(evnt) {
        addDeviceToTable(evnt.detail.device, false);
    });

    window.addEventListener('deviceClosed', function(evnt) {
        removeDeviceFromTable(evnt.detail.device.address, false);
    });

    // Scan event bindings
    $('#startScan').click(function() {
        $('#devices tbody').html('<tr><td id="no-devices" colspan="3">No devices found</td>');
        scan(false);
    });

    // Journey event bindings
    $('#startJourney').click(function() {
        getDeviceForJourney()
            .then(function(device) {
                navigator.notification.alert(
                    'Connected to button',
                    function() {
                        $('#startJourney').hide();
                        $('#stopJourney').show();                   
                    });
            })
            .catch(function() {
                navigator.notification.alert(
                    'Unable to connect to button',
                    function() {});
            });
    });

    $('#stopJourney').click(function() {
        $('#startJourney').show();
        $('#stopJourney').hide();

        window.removeEventListener(
            'deviceDisconnected',
            reconnectDeviceForJourney);

        if (window._journey_reconnect_timeout) {
            clearTimeout(window._journey_reconnect_timeout);
        }

        disconnect(window._device.address).then(close);
    });

    var params = {
        'request': true,
        'statusReceiver': true,
        'restoreKey': 'sensibel',
    };

    bluetoothle.initialize(initialized, params);    
}

// Device ready listener
document.addEventListener('deviceready', deviceReady, false);
